/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
define("plugin-plan-ownership/owner-autocomplete", ["jquery", "underscore", "marionette"], ($, _, Marionette) => {
    "use strict";

    const URL = `${AJS.contextPath()}/rest/api/latest/search/users`;
    const PAGE_SIZE = 10;
    const DEBOUNCE_PERIOD = 250;
    const BOT_ACCOUNT_RE = /.*-bot.*/;

    const notABot = (user) => {
        return !BOT_ACCOUNT_RE.exec(user.id);
    };

    const processResults = function (data, page) {
        const more = page * PAGE_SIZE < data.size;
        const results = data.searchResults.filter(notABot).map((user) => ({
            id: user.id,
            text: user.id,
            fullName: user.searchEntity.fullName,
        }));

        return {
            results,
            more,
        };
    };

    const prepareData = function (searchTerm, page) {
        return {
            searchTerm,
            "start-index": Math.max((page - 1) * PAGE_SIZE, 0),
            "max-results": PAGE_SIZE,
        };
    };

    return Marionette.ItemView.extend({
        template: false,

        initialize(options) {
            this.options = options;
        },

        onRender() {
            this.initSelect2();
            this.setInitialData();
        },

        initSelect2() {
            this.$el.auiSelect2({
                minimumInputLength: 2,
                placeholder: this.options.placeholder,
                ajax: {
                    url: URL,
                    dataType: "json",
                    quietMillis: DEBOUNCE_PERIOD,
                    data: prepareData,
                    results: processResults,
                },
                formatResult: (entity, container, query, escapeMarkup) => {
                    return escapeMarkup(`${entity.id} (${entity.fullName})`);
                },
            });
        },

        setInitialData() {
            const currentOwner = this.$el.val();
            if (currentOwner) {
                this.$el.auiSelect2("data", {
                    id: currentOwner,
                    text: currentOwner,
                });
            }
        },
    });
});
