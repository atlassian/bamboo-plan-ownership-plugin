${webResourceManager.requireResourcesForContext("bamboo-plan-ownership")}

[@ui.bambooSection titleKey="planownership.bamboo.plugin.plan.config.title"]
	[@s.textfield labelKey='planownership.bamboo.plugin.plan.config.owner'
                        name='custom.planownership.bamboo.plugin.plan.config.ownerOfBuild'
                        description=i18n.getText("planownership.bamboo.plugin.plan.config.description")
                        cssClass='long-field owner-autocomplete' /]
    <script>
        require(['jquery', 'plugin-plan-ownership/owner-autocomplete'], function($, OwnerAutocomplete){
            return new OwnerAutocomplete({
                                            el: $('.owner-autocomplete'),
                                            placeholder: "[@s.text name='planownership.bamboo.plugin.plan.config.placeholder'/]",
                                        }).render();
        });
    </script>
[/@ui.bambooSection]
