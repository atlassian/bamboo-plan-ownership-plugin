<html>
<head>
    <meta name="decorator" content="atl.admin">
    <title>[@s.text name='planownership.bamboo.plugin.admin.title' /]</title>
</head>
<body>
[@ui.bambooSection titleKey='planownership.bamboo.plugin.admin.title']
    [@s.form action="configPlanOwnershipEnabler.action" method="post" class="aui"]
        [@s.checkbox id='enableEnforcement_id' labelKey='planownership.bamboo.plugin.admin.enableEnforcement' name='enableEnforcement' /]
        [@s.checkbox id='enablePlanDisablement_id' labelKey='planownership.bamboo.plugin.admin.enablePlanDisablement' name='enablePlanDisablement' /]
        [@s.submit value="Save" theme="simple" cssClass="aui-button"/]
    [/@s.form]
[/@ui.bambooSection]

</body>
</html>
