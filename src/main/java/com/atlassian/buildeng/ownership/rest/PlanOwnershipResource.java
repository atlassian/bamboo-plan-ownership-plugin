/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership.rest;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.exception.NotFoundException;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.buildeng.ownership.PlanOwnershipValidatorService;
import com.atlassian.buildeng.ownership.RestSetOwnerRequest;
import com.atlassian.buildeng.ownership.config.PlanOwnershipConfigService;
import com.atlassian.buildeng.ownership.plan.PlanOwnershipPlanConfigurationPlugin;
import java.util.HashMap;
import java.util.Map;
import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/owner")
@Produces({MediaType.APPLICATION_JSON})
@Singleton
public class PlanOwnershipResource {

    private final PlanOwnershipConfigService planOwnershipConfigService;

    private final PlanOwnershipValidatorService planOwnershipValidatorService;

    private final PlanManager planManager;

    private final CachedPlanManager cachedPlanManager;

    private final BuildDefinitionManager buildDefinitionManager;

    /**
     * The REST resource for getting/setting ownership data for plans.
     */
    @Inject
    public PlanOwnershipResource(
            PlanOwnershipConfigService planOwnershipConfigService,
            PlanOwnershipValidatorService planOwnershipValidatorService,
            PlanManager planManager,
            BuildDefinitionManager buildDefinitionManager,
            CachedPlanManager cachedPlanManager) {
        this.planOwnershipConfigService = planOwnershipConfigService;
        this.planManager = planManager;
        this.buildDefinitionManager = buildDefinitionManager;
        this.cachedPlanManager = cachedPlanManager;
        this.planOwnershipValidatorService = planOwnershipValidatorService;
    }

    /**
     * @return JSON data relating to current plan ownership.
     */
    @GET
    @Path("/{planKey}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getOwner(@PathParam("planKey") String planKey) {
        Plan plan = getPlan(planKey);

        Map<String, Object> result = new HashMap<>();

        result.put("plan.ownership.stopPlans", planOwnershipConfigService.isEnforcementEnabled());
        result.put("plan.ownership.owner", planOwnershipValidatorService.getOwnerFromConfig(plan));
        result.put("plan.ownership.currentUserCanEditPlan", planOwnershipValidatorService.userCanEditPlan(plan));
        return Response.ok(result).build();
    }

    /**
     * @return JSON data of the all plans on the server and their owners.
     */
    @GET
    @Path("/owners")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAllOwners() {

        Map<String, Object> owners = new HashMap<>();

        // iterate over all plans (but not branches!)
        for (final ImmutablePlan plan : cachedPlanManager.getPlans(ImmutableTopLevelPlan.class)) {
            owners.put(plan.getKey(), planOwnershipValidatorService.getOwnerFromConfig(plan));
        }

        return Response.ok(owners).build();
    }

    /**
     * Sets a new valid plan owner.
     */
    @POST
    @Path("/{planKey}")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response setOwner(@PathParam("planKey") String planKey, RestSetOwnerRequest restSetOwnerRequest) {

        try {
            validateOwner(restSetOwnerRequest);
        } catch (InvalidOwnerException e) {
            return Response.serverError().entity(e.getMessage()).build();
        }

        Map<String, String> customConfig;
        try {
            customConfig = getConfig(planKey);
        } catch (IllegalArgumentException e) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(e.getMessage())
                    .build();
        } catch (NotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
        customConfig.put(PlanOwnershipPlanConfigurationPlugin.OWNER_OF_PLAN, restSetOwnerRequest.getOwner());
        setConfig(planKey, customConfig);

        return Response.ok().build();
    }

    private void setConfig(String planKey, Map<String, String> customConfig) {
        Plan plan = getPlan(planKey);

        BuildDefinition buildDefinition = plan.getBuildDefinition();
        buildDefinition.setCustomConfiguration(customConfig);
        buildDefinitionManager.savePlanAndDefinition(plan, buildDefinition);
    }

    private Map<String, String> getConfig(String planKey) throws IllegalArgumentException, NotFoundException {
        Plan plan = getPlan(planKey);

        return plan.getBuildDefinition().getCustomConfiguration();
    }

    Plan getPlan(String planKey) {
        PlanKey key;
        try {
            key = PlanKeys.getPlanKey(planKey);
            if (PlanKeys.isJobKey(key)) {
                key = PlanKeys.getChainKeyFromJobKey(key);
            }
        } catch (IllegalArgumentException e) {
            throw e;
        }

        Plan plan = planManager.getPlanByKey(key);
        if (plan == null) {
            throw new NotFoundException();
        }
        return plan;
    }

    private void validateOwner(RestSetOwnerRequest restSetOwnerRequest) throws InvalidOwnerException {
        boolean valid = planOwnershipValidatorService.isSuggestedOwnerValid(restSetOwnerRequest.getOwner());
        if (!valid) {
            throw new InvalidOwnerException(
                    planOwnershipValidatorService.getReasonForSuggestedOwner(restSetOwnerRequest.getOwner()));
        }
    }

    static class InvalidOwnerException extends Exception {
        public InvalidOwnerException(String message) {
            super(message);
        }
    }
}
