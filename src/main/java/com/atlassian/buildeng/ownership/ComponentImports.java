package com.atlassian.buildeng.ownership;

import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.BambooImport;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;

public class ComponentImports {
    @BambooImport
    private AdministrationConfigurationAccessor administrationConfigurationAccessor;

    @BambooImport
    private PlanManager planManager;

    @BambooImport
    private BambooUserManager bambooUserManager;

    @BambooImport
    private CachedPlanManager cachedPlanManager;

    @BambooImport
    private BambooPermissionManager bambooPermissionManager;

    @BambooImport
    private BuildLoggerManager buildLoggerManager;

    @BambooImport
    private BuildDefinitionManager buildDefinitionManager;

    @BambooImport
    private PluginSettingsFactory pluginSettingsFactory;
}
