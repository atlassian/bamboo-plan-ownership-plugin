/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership.config;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import javax.inject.Inject;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@Component
public class PlanOwnershipConfigService {
    public static final String ENABLE_ENFORCEMENT = "com.atlassian.buildeng.ownership.enableEnforcement";
    public static final String DISABLE_PLANS = "com.atlassian.buildeng.ownership.enablePlanDisablement";

    private PluginSettings pluginSettings;

    @Inject
    public PlanOwnershipConfigService(@NotNull PluginSettingsFactory pluginSettingsFactory) {
        this.pluginSettings = pluginSettingsFactory.createGlobalSettings();
    }

    public void setEnableEnforcement(boolean enabled) {
        pluginSettings.put(ENABLE_ENFORCEMENT, String.valueOf(enabled));
    }

    public boolean isEnforcementEnabled() {
        return Boolean.valueOf(getPluginSettingsValue(ENABLE_ENFORCEMENT));
    }

    public void setEnablePlanDisablement(boolean enabled) {
        pluginSettings.put(DISABLE_PLANS, String.valueOf(enabled));
    }

    public boolean isPlanDisablementEnabled() {
        return Boolean.valueOf(getPluginSettingsValue(DISABLE_PLANS));
    }

    private String getPluginSettingsValue(@NotNull final String key) {
        final Object value = this.pluginSettings.get(key);
        if (value != null) {
            return value.toString();
        } else {
            return null;
        }
    }
}
