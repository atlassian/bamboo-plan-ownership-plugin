/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership.config;

import com.atlassian.bamboo.configuration.GlobalAdminAction;
import org.jetbrains.annotations.NotNull;

public class PlanOwnershipConfig extends GlobalAdminAction {
    private boolean enableEnforcement;
    private boolean enablePlanDisablement;

    private final PlanOwnershipConfigService planOwnershipConfigService;

    public PlanOwnershipConfig(@NotNull final PlanOwnershipConfigService planOwnershipConfigService) {
        this.planOwnershipConfigService = planOwnershipConfigService;
    }

    /**
     * Saves plugin configuration.
     */
    public String doSave() {
        planOwnershipConfigService.setEnableEnforcement(enableEnforcement);
        planOwnershipConfigService.setEnablePlanDisablement(enablePlanDisablement);
        return SUCCESS;
    }

    public boolean getEnableEnforcement() {
        return planOwnershipConfigService.isEnforcementEnabled();
    }

    public void setEnableEnforcement(boolean enableEnforcement) {
        this.enableEnforcement = enableEnforcement;
    }

    public boolean getEnablePlanDisablement() {
        return planOwnershipConfigService.isPlanDisablementEnabled();
    }

    public void setEnablePlanDisablement(boolean enablePlanDisablement) {
        this.enablePlanDisablement = enablePlanDisablement;
    }
}
