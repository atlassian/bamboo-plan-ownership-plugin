package com.atlassian.buildeng.ownership;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.chains.ChainExecution;
import com.atlassian.bamboo.chains.plugins.PreChainAction;
import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.buildeng.ownership.config.PlanOwnershipConfigService;
import javax.inject.Inject;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PlanDisablerQueuedPreChainAction implements PreChainAction {

    private static final Logger logger = LoggerFactory.getLogger(PlanDisablerQueuedPreChainAction.class);

    private final PlanManager planManager;
    private final PlanOwnershipConfigService planOwnershipConfigService;
    private final PlanOwnershipValidatorService planOwnershipValidatorService;
    private final AdministrationConfigurationAccessor administrationConfigurationAccessor;
    private final BuildLoggerManager buildLoggerManager;
    private final PlanExecutionManager planExecutionManager;

    @Inject
    public PlanDisablerQueuedPreChainAction(
            @NotNull PlanManager planManager,
            @NotNull AdministrationConfigurationAccessor administrationConfigurationAccessor,
            @NotNull BuildLoggerManager buildLoggerManager,
            @NotNull PlanOwnershipValidatorService planOwnershipValidatorService,
            @NotNull PlanOwnershipConfigService planOwnershipConfigService,
            @NotNull PlanExecutionManager planExecutionManager) {
        this.planManager = planManager;
        this.administrationConfigurationAccessor = administrationConfigurationAccessor;
        this.planOwnershipConfigService = planOwnershipConfigService;
        this.planOwnershipValidatorService = planOwnershipValidatorService;
        this.buildLoggerManager = buildLoggerManager;
        this.planExecutionManager = planExecutionManager;
    }

    @Override
    public void execute(@NotNull ImmutableChain chain, @NotNull ChainExecution chainExecution) {
        PlanKey planKey = chain.getPlanKey();

        if (!planOwnershipConfigService.isEnforcementEnabled()
                || planOwnershipValidatorService.isExistingOwnerValid(planKey.getKey())) {
            return;
        }

        PlanResultKey planResultKey = chainExecution.getPlanResultKey();
        String reason = planOwnershipValidatorService.getReasonForExistingOwner(planKey.getKey());
        addReasonToBuildLogger(planResultKey, reason, planKey);

        logger.info("Plan Ownership Plugin stopped build: {}", planResultKey);
        Plan parentPlan = planManager.getPlanByKey(planKey);
        if (planOwnershipConfigService.isPlanDisablementEnabled()) {
            parentPlan.setSuspendedFromBuilding(true);
            planManager.savePlan(parentPlan);
            logger.info("Plan Ownership Plugin disabled: {}", planKey.getKey());
        }

        // userName is not used and can be null
        planExecutionManager.stopPlan(planResultKey, true, null);

        chainExecution.getStages().forEach(stage -> stage.getBuilds().forEach(buildExecution -> {
            BuildContext buildContext = buildExecution.getBuildContext();
            buildContext.getErrorCollection().addError("owner", reason);
            buildContext.getCurrentResult().setBuildState(BuildState.UNKNOWN);
        }));
    }

    private void addReasonToBuildLogger(PlanResultKey planResultKey, String reason, PlanKey parentPlanKey) {
        String baseUrl = this.administrationConfigurationAccessor
                .getAdministrationConfiguration()
                .getBaseUrl();

        BuildLogger buildLogger = buildLoggerManager.getLogger(planResultKey);
        buildLogger.addErrorLogEntry("This build has been stopped because: ");
        buildLogger.addErrorLogEntry("\t" + reason);
        buildLogger.addErrorLogEntry(
                "Please correct this via the Other configuration tab of your plan: "
                        + baseUrl + "/chain/admin/config/editChainMiscellaneous.action?buildKey="
                        + parentPlanKey.getKey()
                        + "\nSee https://bitbucket.org/atlassian/bamboo-plan-ownership-plugin/src/master/README.md for details.");
    }
}
