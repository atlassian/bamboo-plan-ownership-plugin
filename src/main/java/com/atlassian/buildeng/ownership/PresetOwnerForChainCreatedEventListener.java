/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.event.ChainCreatedEvent;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.security.acegi.acls.HibernateMutableAclService;
import com.atlassian.bamboo.security.acegi.acls.HibernateObjectIdentityImpl;
import com.atlassian.buildeng.ownership.plan.PlanOwnershipPlanConfigurationPlugin;
import com.atlassian.event.api.EventListener;
import java.util.Map;
import javax.inject.Inject;
import org.acegisecurity.acls.Acl;
import org.acegisecurity.acls.sid.PrincipalSid;
import org.jetbrains.annotations.NotNull;

public class PresetOwnerForChainCreatedEventListener {

    private final PlanOwnershipValidatorService planOwnershipValidatorService;
    private final PlanManager planManager;
    private final HibernateMutableAclService aclService;
    private final BuildDefinitionManager buildDefinitionManager;

    /**
     * An event queue listener that tries to set owners on new plans.
     */
    @Inject
    public PresetOwnerForChainCreatedEventListener(
            @NotNull PlanManager planManager,
            @NotNull PlanOwnershipValidatorService planOwnershipValidatorService,
            @NotNull HibernateMutableAclService aclService,
            @NotNull BuildDefinitionManager buildDefinitionManager) {
        this.planManager = planManager;
        this.planOwnershipValidatorService = planOwnershipValidatorService;
        this.aclService = aclService;
        this.buildDefinitionManager = buildDefinitionManager;
    }

    /**
     * When a plan is created by a user that can be owner, preset the value.
     * This typically happens when plans are manually created.
     *
     * @param event fired when creating new plan
     */
    @EventListener
    public void onEvent(ChainCreatedEvent event) {
        Plan plan = planManager.getPlanByKey(event.getPlanKey(), Plan.class);
        if (plan != null && !plan.hasMaster()) {
            Acl acl = aclService.readAclById(new HibernateObjectIdentityImpl(plan));
            if (acl != null && acl.getOwner() instanceof PrincipalSid) {
                PrincipalSid ps = (PrincipalSid) acl.getOwner();
                String owner = ps.getPrincipal();
                if (planOwnershipValidatorService.isSuggestedOwnerValid(owner)) {
                    BuildDefinition buildDefinition = plan.getBuildDefinition();
                    Map<String, String> cd = buildDefinition.getCustomConfiguration();
                    cd.put(PlanOwnershipPlanConfigurationPlugin.OWNER_OF_PLAN, owner);
                    buildDefinition.setCustomConfiguration(cd);
                    buildDefinitionManager.savePlanAndDefinition(plan, buildDefinition);
                }
            }
        }
    }
}
