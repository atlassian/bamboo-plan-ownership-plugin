/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership.plan;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.plan.configuration.MiscellaneousPlanConfigurationPlugin;
import com.atlassian.bamboo.specs.api.builders.owner.PlanOwner;
import com.atlassian.bamboo.specs.api.exceptions.PropertiesValidationException;
import com.atlassian.bamboo.specs.api.model.owner.PlanOwnerProperties;
import com.atlassian.bamboo.specs.api.validators.common.ValidationContext;
import com.atlassian.bamboo.specs.yaml.BambooYamlParserUtils;
import com.atlassian.bamboo.specs.yaml.MapNode;
import com.atlassian.bamboo.specs.yaml.Node;
import com.atlassian.bamboo.specs.yaml.StringNode;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.bamboo.utils.error.SimpleErrorCollection;
import com.atlassian.bamboo.v2.build.BaseBuildConfigurationAwarePlugin;
import com.atlassian.bamboo.v2.build.ImportExportAwarePlugin;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.atlassian.buildeng.ownership.PlanOwnershipValidatorService;
import com.atlassian.buildeng.ownership.config.PlanOwnershipConfigService;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class PlanOwnershipPlanConfigurationPlugin extends BaseBuildConfigurationAwarePlugin
        implements MiscellaneousPlanConfigurationPlugin, ImportExportAwarePlugin<PlanOwner, PlanOwnerProperties> {
    private static final String YAML_KEY = "owner";
    public static final String OWNER_OF_PLAN = PlanOwnerProperties.OWNER_OF_PLAN;

    private PlanOwnershipConfigService planOwnershipConfigService;

    private PlanOwnershipValidatorService planOwnershipValidatorService;

    public void setPlanOwnershipConfigService(PlanOwnershipConfigService planOwnershipConfigService) {
        this.planOwnershipConfigService = planOwnershipConfigService;
    }

    public void setPlanOwnershipValidatorService(PlanOwnershipValidatorService planOwnershipValidatorService) {
        this.planOwnershipValidatorService = planOwnershipValidatorService;
    }

    @Override
    public boolean isApplicableTo(@NotNull final ImmutablePlan plan) {
        return plan instanceof ImmutableTopLevelPlan;
    }

    @Override
    protected void populateContextForEdit(
            @NotNull Map<String, Object> context, @NotNull BuildConfiguration buildConfiguration, @Nullable Plan plan) {
        super.populateContextForView(context, plan);
    }

    @NotNull
    @Override
    public ErrorCollection validate(@NotNull final BuildConfiguration buildConfiguration) {
        ErrorCollection errorCollection = new SimpleErrorCollection();

        String suggestedOwner = buildConfiguration.getString(OWNER_OF_PLAN);

        if (!planOwnershipConfigService.isEnforcementEnabled()) {
            return errorCollection;
        } else {
            if (!planOwnershipValidatorService.isSuggestedOwnerValid(suggestedOwner)) {
                errorCollection.addError(
                        OWNER_OF_PLAN, planOwnershipValidatorService.getReasonForSuggestedOwner(suggestedOwner));
            }
            return errorCollection;
        }
    }

    @NotNull
    @Override
    public Set<String> getConfigurationKeys() {
        return Collections.singleton(OWNER_OF_PLAN);
    }

    @NotNull
    @Override
    public PlanOwner toSpecsEntity(HierarchicalConfiguration buildConfiguration) {
        return new PlanOwner(buildConfiguration.getString(OWNER_OF_PLAN));
    }

    @Override
    public void addToBuildConfiguration(
            PlanOwnerProperties specsProperties, @NotNull HierarchicalConfiguration buildConfiguration) {
        specsProperties.validate();
        if (planOwnershipConfigService.isEnforcementEnabled()) {
            if (!planOwnershipValidatorService.isSuggestedOwnerValid(specsProperties.getOwner())) {
                throw new PropertiesValidationException(
                        planOwnershipValidatorService.getReasonForSuggestedOwner(specsProperties.getOwner()));
            }
        }
        buildConfiguration.setProperty(OWNER_OF_PLAN, specsProperties.getOwner());
    }

    @Nullable
    @Override
    public PlanOwner fromYaml(@NotNull Node node) throws PropertiesValidationException {
        if (node instanceof MapNode) {
            MapNode mapNode = (MapNode) node;
            final Optional<StringNode> ownerOptional = mapNode.getOptionalString(YAML_KEY);
            if (ownerOptional.isPresent()
                    && StringUtils.isNotBlank(ownerOptional.get().get())) {
                return new PlanOwner(ownerOptional.get().get());
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Node toYaml(@NotNull PlanOwnerProperties specsProperties) {
        final Map<String, String> result = new HashMap<>();
        result.put(YAML_KEY, specsProperties.getOwner());
        return BambooYamlParserUtils.asNode(result, ValidationContext.of(YAML_KEY));
    }
}
