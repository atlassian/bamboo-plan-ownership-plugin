/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership;

import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.plugin.web.model.WebPanel;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;

public class JobSummaryPanel implements WebPanel {

    private final PlanSummaryPanel planSummaryPanel;

    public JobSummaryPanel(PlanSummaryPanel planSummaryPanel) {
        this.planSummaryPanel = planSummaryPanel;
    }

    @Override
    public String getHtml(Map<String, Object> context) {
        ImmutableJob job = (ImmutableJob) context.get("plan");
        return planSummaryPanel.buildMessage(job.getParent());
    }

    @Override
    public void writeHtml(Writer writer, Map<String, Object> context) throws IOException {
        writer.append(getHtml(context));
    }
}
