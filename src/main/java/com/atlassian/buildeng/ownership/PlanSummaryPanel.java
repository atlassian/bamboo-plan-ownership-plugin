/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership;

import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.buildeng.ownership.config.PlanOwnershipConfigService;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.web.model.WebPanel;
import java.io.IOException;
import java.io.Writer;
import java.util.Map;
import javax.inject.Inject;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;

@ExportAsService
@Component
public class PlanSummaryPanel implements WebPanel {

    private AdministrationConfigurationAccessor administrationConfigurationAccessor;

    private final PlanOwnershipConfigService planOwnershipConfigService;

    private final PlanOwnershipValidatorService planOwnershipValidatorService;

    /**
     * Adds UI text to plan pages missing valid owners with details of why they will not run.
     */
    @Inject
    public PlanSummaryPanel(
            AdministrationConfigurationAccessor administrationConfigurationAccessor,
            PlanOwnershipConfigService planOwnershipConfigService,
            PlanOwnershipValidatorService planOwnershipValidatorService) {
        this.administrationConfigurationAccessor = administrationConfigurationAccessor;
        this.planOwnershipConfigService = planOwnershipConfigService;
        this.planOwnershipValidatorService = planOwnershipValidatorService;
    }

    @Override
    public String getHtml(Map<String, Object> context) {
        ImmutableChain plan = (ImmutableChain) context.get("plan");
        return buildMessage(plan);
    }

    /**
     * @param plan The plan to evaluate.
     * @return Empty string if the plan has a valid owner, otherwise raw HTML of error message.
     */
    @NotNull
    public String buildMessage(ImmutableChain plan) {
        String baseUrl = this.administrationConfigurationAccessor
                .getAdministrationConfiguration()
                .getBaseUrl();

        if (planOwnershipConfigService.isEnforcementEnabled()
                && !planOwnershipValidatorService.isExistingOwnerValid(plan.getKey())) {
            String planKey = (plan.getMaster() == null)
                    ? plan.getKey()
                    : plan.getMaster().getKey();

            String reason = planOwnershipValidatorService.getReasonForExistingOwner(planKey);

            return "<h2>The next time this build is run it will be stopped because:</h2>"
                    + "<h3><font color=\"red\">" + reason + "</font></h3>"
                    + "<br>"
                    + "<p>Please add an owner via the "
                    + "<a href=" + baseUrl + "/chain/admin/config/editChainMiscellaneous.action?buildKey=" + planKey
                    + ">Other configuration tab of your plan</a>."
                    + "<br>"
                    + "For more information, please see "
                    + "<a href=" + "https://bitbucket.org/atlassian/bamboo-plan-ownership-plugin/src/master/README.md"
                    + ">README.md</a></p>";
        }
        return "";
    }

    @Override
    public void writeHtml(Writer writer, Map<String, Object> context) throws IOException {
        writer.append(getHtml(context));
    }
}
