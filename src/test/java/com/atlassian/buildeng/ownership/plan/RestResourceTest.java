/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership.plan;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.buildeng.ownership.PlanOwnershipValidatorService;
import com.atlassian.buildeng.ownership.RestSetOwnerRequest;
import com.atlassian.buildeng.ownership.config.PlanOwnershipConfigService;
import com.atlassian.buildeng.ownership.rest.PlanOwnershipResource;
import com.atlassian.user.EntityException;
import com.google.common.collect.Maps;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.Response;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

/**
 * Testing {@link PlanOwnershipResource}
 */
@RunWith(MockitoJUnitRunner.class)
public class RestResourceTest extends TestCase {
    @InjectMocks
    @Spy
    private PlanOwnershipResource planOwnershipResource;

    @Mock
    private PlanManager planManager;

    @Mock
    private BuildDefinitionManager buildDefinitionManager;

    @Mock
    private PlanOwnershipConfigService planOwnershipConfigService;

    @Mock
    private PlanOwnershipValidatorService planOwnershipValidatorService;

    @Mock
    private CachedPlanManager cachedPlanManager;

    @Before
    public final void setUp() throws EntityException {}

    @Test
    public void testGetOwner() throws Exception {

        Plan plan = mock(Plan.class);
        PlanKey planKey = PlanKeys.getPlanKey("AAA-BBB");
        when(planManager.getPlanByKey(planKey)).thenReturn(plan);

        when(planOwnershipConfigService.isEnforcementEnabled()).thenReturn(true);
        when(planOwnershipValidatorService.getOwnerFromConfig(plan)).thenReturn("123fake");

        Map<String, Object> expectedResponse = Maps.newHashMap();
        expectedResponse.put("plan.ownership.stopPlans", true);
        expectedResponse.put("plan.ownership.owner", "123fake");
        expectedResponse.put("plan.ownership.currentUserCanEditPlan", false);
        Response response = planOwnershipResource.getOwner("AAA-BBB");
        assertEquals(expectedResponse, response.getEntity());
    }

    @Test
    public void testSetOwner() throws Exception {

        String username = "123fake";

        RestSetOwnerRequest restSetOwnerRequest = new RestSetOwnerRequest(username);

        Plan plan = mock(Plan.class);
        PlanKey planKey = PlanKeys.getPlanKey("AAA-BBB");
        when(planManager.getPlanByKey(planKey)).thenReturn(plan);
        BuildDefinition definition = mock(BuildDefinition.class);
        when(plan.getBuildDefinition()).thenReturn(definition);
        when(definition.getCustomConfiguration()).thenReturn(Maps.newHashMap());

        doNothing().when(buildDefinitionManager).savePlanAndDefinition(isA(Plan.class), isA(BuildDefinition.class));

        when(planOwnershipValidatorService.isSuggestedOwnerValid(username)).thenReturn(true);
        Response response = planOwnershipResource.setOwner("AAA-BBB", restSetOwnerRequest);
        assertEquals(200, response.getStatus());
    }

    @Test
    public void testGetAllOwners() throws Exception {

        Plan plan1 = mock(Plan.class);
        Plan plan2 = mock(Plan.class);

        List plans = new ArrayList();
        plans.add(plan1);
        plans.add(plan2);
        when(plan1.getKey()).thenReturn("PL-AN1");
        when(plan2.getKey()).thenReturn("PL-AN2");

        when(planOwnershipValidatorService.getOwnerFromConfig(plan1)).thenReturn("123fake");
        when(planOwnershipValidatorService.getOwnerFromConfig(plan2)).thenReturn("456stillFake");

        when(cachedPlanManager.getPlans(ImmutableTopLevelPlan.class)).thenReturn(plans);

        Map<String, Object> expectedResponse = Maps.newHashMap();
        expectedResponse.put("PL-AN1", "123fake");
        expectedResponse.put("PL-AN2", "456stillFake");

        Response response = planOwnershipResource.getAllOwners();
        assertEquals(expectedResponse, response.getEntity());
    }
}
