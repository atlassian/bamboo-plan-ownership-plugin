/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership.plan;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.buildeng.ownership.PlanOwnershipValidatorService;
import com.google.common.collect.Maps;
import java.util.Map;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlanOwnershipValidatorServiceTest {

    @InjectMocks
    @Spy
    private PlanOwnershipValidatorService planOwnershipValidatorService;

    @Mock
    private BambooUserManager bambooUserManager;

    @Mock
    private BambooUser bambooUser;

    @Mock
    private CachedPlanManager planManager;

    @Mock
    private Plan plan;

    @Mock
    private BuildDefinition buildDefinition;

    @Before
    public void SetUp() {
        when(planManager.getPlanByKey(PlanKeys.getPlanKey("abc-123"))).thenReturn(plan);
        when(plan.getBuildDefinition()).thenReturn(buildDefinition);
        when(plan.getName()).thenReturn("My plan");
    }

    @Test
    public void testNoOwner() {
        Map<String, String> customConfiguration = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfiguration);
        assertFalse(planOwnershipValidatorService.isExistingOwnerValid("abc-123"));
    }

    @Test
    public void testNoOwnerReason() {
        Map<String, String> customConfiguration = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfiguration);
        assertEquals(
                PlanOwnershipValidatorService.NO_OWNER,
                planOwnershipValidatorService.getReasonForExistingOwner("abc-123"));
    }

    @Test
    public void testNonExistingOwner() {
        when(bambooUserManager.getBambooUser("trogdorTheBurninator")).thenReturn(null);
        assertFalse(planOwnershipValidatorService.isSuggestedOwnerValid("trogdorTheBurninator"));
    }

    @Test
    public void testNonExistingOwnerReason() {
        when(bambooUserManager.getBambooUser("trogdorTheBurninator")).thenReturn(null);
        assertEquals(
                String.format(PlanOwnershipValidatorService.DOESNT_EXIST, "trogdorTheBurninator"),
                planOwnershipValidatorService.getReasonForSuggestedOwner("trogdorTheBurninator"));
    }

    @Test
    public void testBotOwner() {
        when(bambooUserManager.getBambooUser("beep-boop-bot")).thenReturn(bambooUser);
        when(bambooUser.getUsername()).thenReturn("beep-boop-bot");
        when(bambooUser.isEnabled()).thenReturn(true);
        assertFalse(planOwnershipValidatorService.isSuggestedOwnerValid("beep-boop-bot"));
    }

    @Test
    public void testBotOwnerReason() {
        when(bambooUserManager.getBambooUser("beep-boop-bot")).thenReturn(bambooUser);
        when(bambooUser.getUsername()).thenReturn("beep-boop-bot");
        when(bambooUser.isEnabled()).thenReturn(true);
        assertEquals(
                String.format(PlanOwnershipValidatorService.BOT_ACCOUNT, "beep-boop-bot"),
                planOwnershipValidatorService.getReasonForSuggestedOwner("beep-boop-bot"));
    }

    @Test
    public void testInactiveOwner() {
        Map<String, String> customConfiguration = Maps.newHashMap();
        customConfiguration.put(PlanOwnershipPlanConfigurationPlugin.OWNER_OF_PLAN, "retired-veteran");
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfiguration);
        when(bambooUserManager.getBambooUser("retired-veteran")).thenReturn(bambooUser);
        when(bambooUser.isEnabled()).thenReturn(false);
        assertFalse(planOwnershipValidatorService.isExistingOwnerValid("abc-123"));
    }

    @Test
    public void testInactiveOwnerReason() {
        Map<String, String> customConfiguration = Maps.newHashMap();
        customConfiguration.put(PlanOwnershipPlanConfigurationPlugin.OWNER_OF_PLAN, "retired-veteran");
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfiguration);
        when(bambooUserManager.getBambooUser("retired-veteran")).thenReturn(bambooUser);
        when(bambooUser.isEnabled()).thenReturn(false);
        assertEquals(
                String.format(PlanOwnershipValidatorService.INACTIVE_USER, "retired-veteran"),
                planOwnershipValidatorService.getReasonForExistingOwner("abc-123"));
    }

    @Test
    public void testValidOwner() {
        Map<String, String> customConfiguration = Maps.newHashMap();
        customConfiguration.put(PlanOwnershipPlanConfigurationPlugin.OWNER_OF_PLAN, "bambi");
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfiguration);
        when(bambooUserManager.getBambooUser("bambi")).thenReturn(bambooUser);
        when(bambooUser.getUsername()).thenReturn("bambi");
        when(bambooUser.isEnabled()).thenReturn(true);
        assertTrue(planOwnershipValidatorService.isExistingOwnerValid("abc-123"));
    }

    @Test
    public void testValidOwnerReason() {
        Map<String, String> customConfiguration = Maps.newHashMap();
        customConfiguration.put(PlanOwnershipPlanConfigurationPlugin.OWNER_OF_PLAN, "bambi");
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfiguration);
        when(bambooUserManager.getBambooUser("bambi")).thenReturn(bambooUser);
        when(bambooUser.getUsername()).thenReturn("bambi");
        when(bambooUser.isEnabled()).thenReturn(true);
        assertEquals("", planOwnershipValidatorService.getReasonForExistingOwner("abc-123"));
    }

    @Test
    public void testValidOwnerFromPlanName() {
        Map<String, String> customConfiguration = Maps.newHashMap();
        when(buildDefinition.getCustomConfiguration()).thenReturn(customConfiguration);
        when(plan.getName()).thenReturn("My plan Owner=bambi");
        when(bambooUserManager.getBambooUser("bambi")).thenReturn(bambooUser);
        when(bambooUser.getUsername()).thenReturn("bambi");
        when(bambooUser.isEnabled()).thenReturn(true);
        assertTrue(planOwnershipValidatorService.isExistingOwnerValid("abc-123"));
    }
}
