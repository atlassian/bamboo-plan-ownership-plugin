package com.atlassian.buildeng.ownership;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import com.atlassian.bamboo.build.BuildLoggerManager;
import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.chains.ChainExecution;
import com.atlassian.bamboo.configuration.AdministrationConfiguration;
import com.atlassian.bamboo.configuration.AdministrationConfigurationAccessor;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanExecutionManager;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.plan.cache.ImmutableChain;
import com.atlassian.buildeng.ownership.config.PlanOwnershipConfigService;
import junit.framework.TestCase;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PlanDisablerQueuedPreChainActionTest extends TestCase {
    @InjectMocks
    @Spy
    private PlanDisablerQueuedPreChainAction action;

    @Mock
    private PlanOwnershipConfigService planOwnershipConfigService;

    @Mock
    private PlanOwnershipValidatorService planOwnershipValidatorService;

    @Mock
    private AdministrationConfigurationAccessor administrationConfigAccessor;

    @Mock
    private PlanManager planManager;

    @Mock
    private BuildLoggerManager buildLoggerManager;

    @Mock
    private BuildLogger buildLogger;

    @Mock
    private Plan parentPlan;

    @Mock
    private AdministrationConfiguration administrationConfiguration;

    @Mock
    private ImmutableChain chain;

    @Mock
    private ChainExecution chainExecution;

    @Mock
    private PlanExecutionManager planExecutionManager;

    @Before
    public void setUp() {
        when(chain.getPlanKey()).thenReturn(PlanKeys.getPlanKey("PLAN-KEY"));
        when(chainExecution.getPlanResultKey()).thenReturn(PlanKeys.getPlanResultKey("PLAN-KEY-123"));
    }

    @Test
    public void testDoNotStopPlanIfOwnerIsOk() {
        action.execute(chain, chainExecution);

        verify(parentPlan, times(0)).setSuspendedFromBuilding(true);
        verifyNoInteractions(planExecutionManager);
    }

    @Test
    public void testStopPlanIfInvalidOwner() {
        when(planOwnershipConfigService.isEnforcementEnabled()).thenReturn(true);
        when(planOwnershipConfigService.isPlanDisablementEnabled()).thenReturn(false);
        when(planOwnershipValidatorService.isExistingOwnerValid(anyString())).thenReturn(false);
        when(buildLoggerManager.getLogger(any(PlanResultKey.class))).thenReturn(buildLogger);
        when(administrationConfigAccessor.getAdministrationConfiguration()).thenReturn(administrationConfiguration);
        when(administrationConfiguration.getBaseUrl()).thenReturn("localhost");
        when(planManager.getPlanByKey(any(PlanKey.class))).thenReturn(parentPlan);

        action.execute(chain, chainExecution);

        verify(parentPlan, times(0)).setSuspendedFromBuilding(true);
        verify(planExecutionManager, times(1)).stopPlan(any(PlanResultKey.class), eq(true), isNull());
    }

    @Test
    public void testDisablePlanIfInvalidOwner() {
        when(planOwnershipConfigService.isEnforcementEnabled()).thenReturn(true);
        when(planOwnershipConfigService.isPlanDisablementEnabled()).thenReturn(true);
        when(planOwnershipValidatorService.isExistingOwnerValid(anyString())).thenReturn(false);
        when(buildLoggerManager.getLogger(any(PlanResultKey.class))).thenReturn(buildLogger);
        when(administrationConfigAccessor.getAdministrationConfiguration()).thenReturn(administrationConfiguration);
        when(administrationConfiguration.getBaseUrl()).thenReturn("localhost");
        when(planManager.getPlanByKey(any(PlanKey.class))).thenReturn(parentPlan);

        action.execute(chain, chainExecution);

        verify(parentPlan, times(1)).setSuspendedFromBuilding(true);
        verify(planExecutionManager, times(1)).stopPlan(any(PlanResultKey.class), eq(true), isNull());
    }
}
