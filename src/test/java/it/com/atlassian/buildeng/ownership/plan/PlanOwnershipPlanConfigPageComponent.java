/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.com.atlassian.buildeng.ownership.plan;

import com.atlassian.bamboo.pageobjects.elements.Select2Element;
import com.atlassian.bamboo.pageobjects.pages.AbstractBambooPage;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.testutils.specs.TestPlanSpecsHelper;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class PlanOwnershipPlanConfigPageComponent extends AbstractBambooPage {
    private final Plan plan;

    @ElementBy(name = "custom.planownership.bamboo.plugin.plan.config.ownerOfBuild")
    private Select2Element ownerSelect;

    @ElementBy(id = "updateChainMiscellaneous_save")
    private PageElement saveButton;

    public PlanOwnershipPlanConfigPageComponent(Plan plan) {
        this.plan = plan;
    }

    public String getUrl() {
        return "chain/admin/config/editChainMiscellaneous.action?buildKey=" + TestPlanSpecsHelper.getPlanKey(plan);
    }

    public PlanOwnershipPlanConfigPageComponent setOwner(String owner) {
        this.ownerSelect.enterValue(owner, choice -> choice.startsWith(owner), owner);
        return this;
    }

    public boolean isUserAvailableAsOwner(String username) {
        return !ownerSelect.openDropDown().type(username).getValues().isEmpty();
    }

    public String getOwner() {
        return ownerSelect.getValue();
    }

    public void save() {
        saveButton.click();
    }

    public PageElement indicator() {
        return ownerSelect;
    }
}
