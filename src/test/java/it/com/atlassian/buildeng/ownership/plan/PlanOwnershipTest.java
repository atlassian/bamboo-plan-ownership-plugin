/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.com.atlassian.buildeng.ownership.plan;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import com.atlassian.bamboo.pageobjects.BambooTestedProduct;
import com.atlassian.bamboo.pageobjects.pages.plan.PlanSummaryPage;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.builders.task.ScriptTask;
import com.atlassian.bamboo.testutils.backdoor.Backdoor;
import com.atlassian.bamboo.testutils.backdoor.model.Result;
import com.atlassian.bamboo.testutils.specs.TestPlanSpecsHelper;
import com.atlassian.bamboo.testutils.user.TestUser;
import com.atlassian.bamboo.webdriver.WebDriverTestEnvironmentData;
import com.atlassian.pageobjects.TestedProductFactory;
import java.util.Arrays;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class PlanOwnershipTest {

    private static final BambooTestedProduct BAMBOO = TestedProductFactory.create(BambooTestedProduct.class);
    private static Backdoor backdoor;
    private static TestUser botUser;

    private Plan plan;
    private PlanKey planKey;

    @BeforeClass
    public static void suiteSetUp() {
        backdoor = new Backdoor(new WebDriverTestEnvironmentData());
        backdoor.asUser(TestUser.ADMIN);
        BAMBOO.fastLogin(TestUser.ADMIN);
        botUser = TestUser.createSimpleUser("milos-bot");
        backdoor.users()
                .createUser(
                        botUser,
                        Arrays.asList(
                                BambooPermission.CREATE,
                                BambooPermission.READ,
                                BambooPermission.WRITE,
                                BambooPermission.BUILD));
    }

    @Before
    public void testSetUp() {
        PlanOwnershipAdminPageComponent adminPage = BAMBOO.visit(PlanOwnershipAdminPageComponent.class);
        adminPage.doWait();
        adminPage.ensureEnablePlanDisablementIsDisabled();
        adminPage.ensureEnableEnforcementIsDisabled();

        backdoor.plans().deleteAllPlans();
        plan = TestPlanSpecsHelper.uniquePlan()
                .stages(new Stage("Default stage")
                        .jobs(new Job("Default job", "JOB")
                                .tasks(new ScriptTask().inlineBody("echo \"hello world\""))));
        planKey = TestPlanSpecsHelper.getPlanKey(plan);
    }

    @Test
    public void testPlanIsStoppedIfEnableEnforcementEnabled() throws Exception {
        PlanOwnershipAdminPageComponent adminPage = BAMBOO.visit(PlanOwnershipAdminPageComponent.class);
        adminPage.doWait();
        adminPage.setEnableEnforcement(true).save();

        backdoor.plans().createOrUpdatePlan(botUser, plan);
        backdoor.plans().triggerBuild(planKey);

        backdoor.plans().waitForCompletedBuild(planKey, 1);

        Result result = backdoor.plans().getBuildResult(planKey, 1);
        assertEquals("NotBuilt", result.getLifeCycleState());
        assertEquals("Unknown", result.getBuildState());
    }

    @Test
    public void testPlanIsDisabledIfEnablePlanDisablementEnabled() throws Exception {
        PlanOwnershipAdminPageComponent adminPage = BAMBOO.visit(PlanOwnershipAdminPageComponent.class);
        adminPage.doWait();
        adminPage.setEnableEnforcement(true).save();
        adminPage.setEnablePlanDisablement(true).save();

        backdoor.plans().createOrUpdatePlan(botUser, plan);
        backdoor.plans().triggerBuild(planKey);

        backdoor.plans().waitForCompletedBuild(planKey, 1);

        PlanSummaryPage summaryPage = BAMBOO.visit(PlanSummaryPage.class, planKey);
        assertFalse(summaryPage.isPlanEnabled());
    }

    @Test
    public void testPlanNotStoppedIfEnableEnforcementDisabled() throws Exception {
        PlanOwnershipAdminPageComponent adminPage = BAMBOO.visit(PlanOwnershipAdminPageComponent.class);
        adminPage.doWait();
        adminPage.setEnableEnforcement(false).save();
        adminPage.setEnablePlanDisablement(true).save();

        backdoor.plans().createPlan(plan);
        backdoor.plans().triggerBuild(planKey);

        backdoor.plans().waitForCompletedBuild(planKey, 1);

        Result result = backdoor.plans().getBuildResult(planKey, 1);
        assertEquals("Finished", result.getLifeCycleState());
        assertEquals("Successful", result.getBuildState());
    }

    @Test
    public void testPlanNotStoppedIfOwnerSpecified() throws Exception {
        PlanOwnershipAdminPageComponent adminPage = BAMBOO.visit(PlanOwnershipAdminPageComponent.class);
        adminPage.doWait();
        adminPage.setEnableEnforcement(false).save();

        backdoor.plans().createPlan(plan);

        PlanOwnershipPlanConfigPageComponent planPage = BAMBOO.visit(PlanOwnershipPlanConfigPageComponent.class, plan);
        planPage.doWait();
        planPage.setOwner("admin");
        planPage.save();

        backdoor.plans().triggerBuild(planKey);
        backdoor.plans().waitForCompletedBuild(planKey, 1);

        Result result = backdoor.plans().getBuildResult(planKey, 1);
        assertEquals("Finished", result.getLifeCycleState());
    }

    @Test
    public void testTryAndSetBotAccountAsOwner() throws Exception {
        PlanOwnershipAdminPageComponent adminPage = BAMBOO.visit(PlanOwnershipAdminPageComponent.class);
        adminPage.doWait();
        adminPage.setEnableEnforcement(true).save();

        backdoor.plans().createPlan(plan);

        PlanOwnershipPlanConfigPageComponent planPage = BAMBOO.visit(PlanOwnershipPlanConfigPageComponent.class, plan);
        planPage.doWait();

        assertFalse(planPage.isUserAvailableAsOwner(botUser.getUsername()));
    }
}
