/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package it.com.atlassian.buildeng.ownership.plan;

import com.atlassian.aui.auipageobjects.AuiCheckbox;
import com.atlassian.bamboo.pageobjects.pages.AbstractBambooPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class PlanOwnershipAdminPageComponent extends AbstractBambooPage {

    @ElementBy(id = "enableEnforcement_id")
    private AuiCheckbox enableEnforcement;

    @ElementBy(id = "enablePlanDisablement_id")
    private AuiCheckbox enablePlanDisablement;

    @ElementBy(id = "configPlanOwnershipEnabler_1")
    private PageElement saveButton;

    public PageElement indicator() {
        return enableEnforcement;
    }

    public String getUrl() {
        return "admin/viewPlanOwnershipEnabler.action";
    }

    public PlanOwnershipAdminPageComponent setEnableEnforcement(boolean enable) {
        if (enable) {
            enableEnforcement.check();
        } else {
            enableEnforcement.uncheck();
        }
        return this;
    }

    public PlanOwnershipAdminPageComponent setEnablePlanDisablement(boolean enable) {
        if (enable) {
            enablePlanDisablement.check();
        } else {
            enablePlanDisablement.uncheck();
        }
        return this;
    }

    public PlanOwnershipAdminPageComponent ensureEnableEnforcementIsDisabled() {
        if (enableEnforcement.isChecked()) {
            enableEnforcement.uncheck();
        }
        return this;
    }

    public PlanOwnershipAdminPageComponent ensureEnablePlanDisablementIsDisabled() {
        if (enablePlanDisablement.isChecked()) {
            enablePlanDisablement.uncheck();
        }
        return this;
    }

    public void save() {
        saveButton.click();
    }
}
