# Bamboo Plan Ownership Plugin

This Bamboo plugin provides an extra plan configuration item on the 'Other' tab where an owner can be specified.

Plan ownership data may be useful for administrators to track down the necessary people to inform of (for example) a platform change that will affect them.

An admin option is also provided to 'enforce' valid plan ownership configuration. If enabled, this will cause any builds that do not have a valid owner set to stop and disable themselves if run.

## Usage

#### REST endpoints

-   `/rest/ownership/1.0/owner/owners` will list all plans with their owners, or `null` if not set. This can be used to measure plan adoption across the Bamboo server.

#### Development loop

##### Java version

This repository's java version is 11. You can set it up on your machine (using `asdf`) with:

```bash
asdf java install latest:temurin-11 # optional if already done
asdf local java latest:temurin-11
```

To run integration tests on the Mac, run Maven with following parameters:

`mvn -Pdev clean verify`

When it reaches the integration tests, it will automatically spawn a new Firefox selenium instance and run the integration tests there.

## Technical details

#### How do I set an owner?

We recommend owners be set via configuration-as-code (e.g. Plan Templates, Bamboo Specs, etc.) but for completeness all options will be covered below.

#### Bamboo UI

Edit your plan and go to the "Other" tab, and scroll down to the "Who owns this plan?" section.

#### Plan Templates

```java
plan(key:'LACH', name:'lach', enabled:'false') {
...
    # Add the following
    planMiscellaneous() {
        planOwnership(owner:'YOUR_USERNAME')
    }
...
}
```

#### Bamboo Specs

See below for the two different ways of setting an owner with Specs.

**pom.xml**

```xml
...
    <dependency>
      <groupId>com.atlassian.buildeng</groupId>
      <artifactId>bamboo-plan-ownership-specs</artifactId>
      <version>1.2.0</version>
    </dependency>
...
```

```java
...
import com.atlassian.bamboo.specs.api.builders.owner.PlanOwner;
...
        .pluginConfigurations(new ConcurrentBuilds()
                .useSystemWideDefault(false),
                new PlanOwner("YOUR_USERNAME"))
...
```

#### YAML Specs

For Bamboo 6.10+

```yaml
version: 2
plan:
    key: PLK
    name: Plan name
    project-key: PRK

other:
    owner: YOUR_OWNER_NAME
```

For Bamboo < 6.10.0 there is no way in the simplified Bamboo YAML Specs to set custom plugin configuration. As a workaround for Plan Ownership, we recognise the following plan name pattern as replacement. Please only use with YAML Specs.

```yaml
version: 2
plan:
    key: PLK
    name: Plan name Owner=YOUR_OWNER_NAME
    project-key: PRK
```

```yaml
project:
    key: PRK
    plan:
        key: PLK
        name: Plan name Owner=YOUR_OWNER_NAME
```

#### REST API

This method is not recommended and only provided to help under unusual circumstances

```bash
  curl -X POST \
  -H 'Content-Type: application/json' \
  -u <YOUR_USERNAME> \
  -d '{"owner": "YOUR_USERNAME"}' \
  --url 'http://YOUR_SERVER/rest/ownership/1.0/owner/YOUR_PROJECT_KEY-YOUR_PLAN_KEY'
```

## Contributing

Pull requests, issues and comments welcome. For pull requests:

-   Add tests for new features and bug fixes
-   Follow the existing style
-   Separate unrelated changes into multiple pull requests

See the existing issues for things to start contributing.
For bigger changes, make sure you start a discussion first by creating an issue and explaining the intended change.

Atlassian requires contributors to sign a Contributor License Agreement, known as a CLA. This serves as a record stating that the contributor is entitled to contribute the code/documentation/translation to the project and is willing to have it used in distributions and derivative works (or is willing to transfer ownership).

Prior to merging your pull requests, please follow the appropriate link below to digitally sign the CLA. The Corporate CLA is for those who are contributing as a member of an organization and the individual CLA is for those contributing as an individual.

-   [Corporate CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=e1c17c66-ca4d-4aab-a953-2c231af4a20b)
-   [Individual CLA](https://na2.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=3f94fbdc-2fbe-46ac-b14c-5d152700ae5d)

## License

Copyright (c) 2019 Atlassian and others. Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassian/bamboo-plan-ownership-plugin/src/master/LICENSE.txt) file.
